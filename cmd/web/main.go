package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/riou.mat/mathilderYogaWebsite/pkg/config"
	"gitlab.com/riou.mat/mathilderYogaWebsite/pkg/handlers"
	"gitlab.com/riou.mat/mathilderYogaWebsite/pkg/render"

	"github.com/alexedwards/scs/v2"
)

// portNumber is a const => can't be changed
const portNumber = ":8080"

var app config.AppConfig
var session *scs.SessionManager

// main is the main application function
func main() {

	err := run()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(fmt.Sprintf("starting application on portNumber %s", portNumber))

	// nil in the listenAndServe func, instead of a handler because we already have a handler above
	srv := &http.Server{
		Addr:    portNumber,
		Handler: routes(&app),
	}

	err = srv.ListenAndServe()
	log.Fatal(err)
}

func run() error {

	app.ProdEnv = false

	session = scs.New()
	// setting up session to last 1h for now
	session.Lifetime = 1 * time.Hour
	// session needs to be killed when the browser to closed
	session.Cookie.Persist = false
	session.Cookie.SameSite = http.SameSiteStrictMode
	session.Cookie.Secure = app.ProdEnv // will be true in prod

	app.Session = session

	tc, err := render.CreateTemplateCache()
	if err != nil {
		log.Fatal("cannot create template cache")
		return err
	}

	app.TemplateCache = tc
	app.UseCache = false

	repo := handlers.NewRepo(&app)
	handlers.NewHandlers(repo)

	render.NewTemplates(&app)

	return nil

}
