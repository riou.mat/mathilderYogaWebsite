# Yoga classes booking website

This is the repo for my yoga classes booking website

- Build in go version 1.19.1
- Uses the [Chi router](https://github.com/go-chi/chi)
- Uses [Alex Edwards SCS](https://github.com/alexedwards/scs) session management
- Uses [noSurf](https://github.com/justinas/nosurf)